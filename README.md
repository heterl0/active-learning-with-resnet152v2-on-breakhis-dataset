# Active Learning with ResNet152V2 on BreakHis Dataset

## Introduction
This repository provides an implementation of Active Learning using the ResNet152V2 pretrained model for classification tasks on the BreakHis dataset. Active Learning is employed as a strategy to select informative samples for labeling, aiming to improve the efficiency of training machine learning models when labeled data is limited or expensive to obtain.

## Dataset
The BreakHis dataset is a histopathological image dataset for breast cancer diagnosis. It consists of 9,109 microscopic images of breast tissue samples, divided into benign and malignant classes, with various magnifications and staining characteristics.

To use this repository, you need to obtain the BreakHis dataset separately. Please refer to the official BreakHis dataset website (e.g., http://web.inf.ufpr.br/vri/databases/breast-cancer-histopathological-database-breakhis/) to download and prepare the dataset.

## Model
The ResNet152V2 model is a deep convolutional neural network architecture that has been pre-trained on a large-scale dataset (e.g., ImageNet). It achieves state-of-the-art performance on various computer vision tasks, including image classification. In this repository, we leverage the pre-trained ResNet152V2 model as a feature extractor.

## Installation
1. Clone the repository: 
```
git clone https://gitlab.com/heterl0/active-learning-with-resnet152v2-on-breakhis-dataset.git
```
cd repository

2. Install the required dependencies:

```
pip install -r requirements.txt
```


3. Download and preprocess the BreakHis dataset:
- Download the dataset from the official BreakHis dataset website. Order you can get it at Kaggle: https://www.kaggle.com/datasets/ambarish/breakhis/data
- Preprocess the dataset by resizing the images, splitting into train and test sets, and organizing the directory structure. All of processing when you run any file *.ipynb
** I do not upload Dataset to this repo **

4. Pretrained model weights:
- Download the pretrained weights for the ResNet152V2 model, I use ImageNet.

## Usage
1. I divided our project into 6 files included: 1 visualization-t-sne, 4 file run Active Learning seperated, and file summarized for this.

2. Active Learning Iterations:
- Run the AL iterations by Open ipynb file:
- entropy.ipynb
- least-confidence.ipynb
- margin.ipynb
- ratio.ipynb

```
Open file and run all
```
- The script will perform the AL process, selecting informative samples for labeling, training the model, and evaluating the performance at each iteration.
- You can adjust the number of iterations, batch size, and selection strategy in the each `*.ipynb` script at function `eval_prioritization_strategy`.

3. Results and Analysis:
- The AL process will generate output files, such as the evaluation metrics save into `data.csv`
- Analyze the performance of the model at each iteration, evaluate the learning curve, and assess the effectiveness of the AL methods, open `Overview.ipynb`.
![Result Ouput](./Charts/output.png)


## Troubleshooting
If you encounter any issues during the installation or execution of the code, please ensure that you have followed the instructions correctly and that all dependencies are installed. If the problem persists, feel free to open an issue on the GitHub repository for assistance.

## License
This project is licensed under the [MIT License](LICENSE).

## Acknowledgements
We would like to acknowledge the authors and contributors of the BreakHis dataset for providing the valuable dataset for breast cancer diagnosis research. We also appreciate the developers of the ResNet152V2 model for their contributions to the field of computer vision.

## Contact Information
For any questions, suggestions, or support, please contact us at hieulv@heterl0.me.